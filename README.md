# HelloVue

HelloVue is a simple TODO list application developed with Vue.js that has been created with the integrated Vue CLI UI.

## Requirements 📋
```
Vue CLI requires Node.js version 8.9 or above (8.11.0+ recommended).
```

## Project setup 🛠️
```
npm install 
```

### Installation of Vue CLI 🔧
```
npm install -g @vue/cli
```

### Run Vue CLI integrated UI 📦
```
vue ui
```
⌨️ with ❤️ by [AlejandroMarín](https://www.linkedin.com/in/amarinol/)